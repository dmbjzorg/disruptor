package entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;


/* Netty 传输数据实体类 */
@Data
@Accessors(chain = true)
public class TranslatorData implements Serializable {

    private String id;
    private String name;
    private String message;     //传输消息具体内容

}
