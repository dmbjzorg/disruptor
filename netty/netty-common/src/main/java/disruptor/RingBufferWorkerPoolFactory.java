package disruptor;


import com.lmax.disruptor.*;
import com.lmax.disruptor.dsl.ProducerType;

import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* Disruptor实现 */
public class RingBufferWorkerPoolFactory {


    private static class SingletonHolder {
        static final RingBufferWorkerPoolFactory instance = new RingBufferWorkerPoolFactory();
    }

    //返回单例模式对象
    public static RingBufferWorkerPoolFactory getInstance(){
        return SingletonHolder.instance;
    }

    private RingBufferWorkerPoolFactory(){

    }

    //用Map管理生产者
    private static Map<String,MessageProductor> productorMap = new ConcurrentHashMap<String, MessageProductor>();

    //用Map管理消费者
    private static Map<String,MessageConsumer> consumerMap = new ConcurrentHashMap<String, MessageConsumer>();

    //序号栅栏
    private SequenceBarrier sequenceBarrier;
    //环形数组
    private RingBuffer<TranslatorDataWrapper> ringBuffer;
    //工作池
    private WorkerPool<TranslatorDataWrapper> workerPool;


    /*
     * 初始化Disruptor并启动
     * @param type生产者类型
     * @param bufferSize 环形缓冲区中创建的元素数
     * @param waitStrategy等待策略
     * @param messageConsumers消费者数组
     */
    public void initAndStart(ProducerType type, int bufferSize, WaitStrategy waitStrategy, MessageConsumer[] messageConsumers) {

        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(8,16,10, TimeUnit.SECONDS,new ArrayBlockingQueue<>(20));

        ringBuffer = RingBuffer.create(type,()->new TranslatorDataWrapper(),bufferSize,new YieldingWaitStrategy());

        //设置序号栅栏(创建序号屏障)
        sequenceBarrier =  ringBuffer.newBarrier();

        //构建多消费者工作池
        workerPool = new WorkerPool<TranslatorDataWrapper>(ringBuffer,sequenceBarrier,new EventExceptionHandler(),messageConsumers);

        //把构建的消费者放入池中
        for (MessageConsumer consumer : messageConsumers) {
            consumerMap.put(consumer.getConsumerId(), consumer);
        }

        //每个消费者的Sequence序号都是单独的，通过WorkPool获取每个消费者的序号然后设置到RingBuffer中
        ringBuffer.addGatingSequences(workerPool.getWorkerSequences());

        //启动工作池
        workerPool.start(threadPool);

    }


    /* 通过ID获取消费者 */
    public MessageProductor getMessageProducer(String producerId){
        MessageProductor messageProducer = productorMap.get(producerId);
        if(null == messageProducer) {
            messageProducer = new MessageProductor(producerId,ringBuffer);
            productorMap.put(producerId, messageProducer);
        }
        return messageProducer;
    }


}
