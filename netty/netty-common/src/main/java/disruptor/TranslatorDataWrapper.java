package disruptor;

import entity.TranslatorData;
import io.netty.channel.ChannelHandlerContext;
import lombok.Data;
import lombok.NoArgsConstructor;


/* Disruptor 传递的数据实体类 */
@Data
@NoArgsConstructor
public class TranslatorDataWrapper {

    private TranslatorData translatorData;      //数据实体类
    private ChannelHandlerContext ctx;          //Netty使用的Ctx对象

}
