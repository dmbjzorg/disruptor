package disruptor;

import com.lmax.disruptor.RingBuffer;
import entity.TranslatorData;
import io.netty.channel.ChannelHandlerContext;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/* 生产者 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MessageProductor {

    private String producerId;
    private RingBuffer<TranslatorDataWrapper> ringBuffer;

    /* 发布数据到 RingBuffer */
    public void onData(TranslatorData data, ChannelHandlerContext ctx) {
        long sequence = ringBuffer.next();
        try {
            TranslatorDataWrapper wapper = ringBuffer.get(sequence);
            wapper.setTranslatorData(data);
            wapper.setCtx(ctx);
        } finally {
            ringBuffer.publish(sequence);
        }
    }

}
