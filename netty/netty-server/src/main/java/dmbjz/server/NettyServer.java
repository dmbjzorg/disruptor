package dmbjz.server;

import factory.MarshallingCodeCFactory;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.marshalling.MarshallingEncoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/* Netty服务端 */
@Component
@Slf4j
public class NettyServer {

    //创建单例模式
    private static class SingletionWsServer{
        static final NettyServer instance = new NettyServer();
    }

    //返回单例模式对象
    public static NettyServer getInstance(){
        return SingletionWsServer.instance;
    }

    private EventLoopGroup bossGroup;
    private EventLoopGroup workGroup;
    private ServerBootstrap bootstrap;
    private ChannelFuture channelFuture;


    /* 构造方法内进行初始化 */
    public NettyServer(){

        bossGroup = new NioEventLoopGroup();
        workGroup = new NioEventLoopGroup();
        bootstrap = new ServerBootstrap();

        bootstrap.group(bossGroup,workGroup)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG,1024)
                //缓存区自适应,最好可以进行手动计算Bytebuf数据包大小来设置缓存区空间
                .option(ChannelOption.RCVBUF_ALLOCATOR, AdaptiveRecvByteBufAllocator.DEFAULT)
                //开启日志记录
                .handler(new LoggingHandler(LogLevel.DEBUG))
                //回调方法
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        //添加Java实体类编解码器
                        pipeline.addLast(MarshallingCodeCFactory.buildMarshallingEncoder());
                        pipeline.addLast(MarshallingCodeCFactory.buildMarshallingDecoder());
                        //添加服务端Handler
                        pipeline.addLast(new ServerHandler());
                    }}
                );

    }

    public void start(){
        //额外线程启动Netty而不是在Main方法中启动，因此无需异步
        //使用Spring容器进行托管，也无需进行关闭
        this.channelFuture = bootstrap.bind(8765);
        log.warn("Netty WebSocket Server启动完毕...");
    }


}
