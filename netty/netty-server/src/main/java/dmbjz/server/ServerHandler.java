package dmbjz.server;

import disruptor.MessageProductor;
import disruptor.RingBufferWorkerPoolFactory;
import entity.TranslatorData;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;


/** 服务端Handler */
public class ServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("通道建立成功");
        super.channelActive(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        /*
            TranslatorData info = (TranslatorData) msg;
            System.out.println("ID："+info.getId()+"，名称："+info.getName()+"，消息："+info.getMessage());

            //创建新数据
            TranslatorData response = new TranslatorData();
            response.setId("resp: " + info.getId());
            response.setName("resp: " + info.getName());
            response.setMessage("resp: " + info.getMessage());

            //返回数据
            ctx.writeAndFlush(response);

         */

        /* 使用disruptor异步执行业务逻辑，将数据发送给消费者即可 */
        TranslatorData info = (TranslatorData) msg;
        String id = "code:seesionId:001";               //同一个用户应使用同一个Id，例如可以使用机器码:sessionId:用户编号作为规则进行生成，这里固定写死
        MessageProductor messageProducer = RingBufferWorkerPoolFactory.getInstance().getMessageProducer(id);
        messageProducer.onData(info,ctx);

    }


}
