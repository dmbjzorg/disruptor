package dmbjz.server;

import disruptor.MessageConsumer;
import disruptor.TranslatorDataWrapper;
import entity.TranslatorData;
import io.netty.channel.ChannelHandlerContext;

public class MessageConsumerImplServer extends MessageConsumer {

	public MessageConsumerImplServer(String consumerId) {
		super(consumerId);
	}

	/* 业务逻辑处理方式 */
	@Override
	public void onEvent(TranslatorDataWrapper event) throws Exception {

		TranslatorData request = event.getTranslatorData();
		ChannelHandlerContext ctx = event.getCtx();

		//业务处理逻辑:
    	System.out.println("Sever端: id= " + request.getId() + ", name= " + request.getName() + ", message= " + request.getMessage());

    	//回送响应信息:
    	TranslatorData response = new TranslatorData();
    	response.setId("resp: " + request.getId());
    	response.setName("resp: " + request.getName());
    	response.setMessage("resp: " + request.getMessage());

    	//写出response响应信息:
    	ctx.writeAndFlush(response);

	}

}
