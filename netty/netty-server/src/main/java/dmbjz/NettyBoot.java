package dmbjz;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.YieldingWaitStrategy;
import com.lmax.disruptor.dsl.ProducerType;
import disruptor.MessageConsumer;
import disruptor.RingBufferWorkerPoolFactory;
import dmbjz.server.MessageConsumerImplServer;
import dmbjz.server.NettyServer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/* Spring加载完成后进行监听执行 */
@Component
public class NettyBoot implements ApplicationListener<ContextRefreshedEvent> {


    /* 当一个ApplicationContext被初始化或刷新触发 */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        //判断当前是否为spring容器初始化完成，防止重复执行
        if(event.getApplicationContext().getParent() == null){
            try {

                //初始化消费者
                MessageConsumer[] conusmers = new MessageConsumer[4];
                for(int i =0; i < conusmers.length; i++) {
                    MessageConsumer messageConsumer = new MessageConsumerImplServer("code:serverId:" + i);
                    conusmers[i] = messageConsumer;
                }
                //初始化Disruptor并启动
                RingBufferWorkerPoolFactory.getInstance().initAndStart(
                        ProducerType.MULTI,
                        1024*1024,
                        //new YieldingWaitStrategy(),
                        new BlockingWaitStrategy(),
                        conusmers);

                //启动Netty服务端
                NettyServer.getInstance().start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
