package com.dmbjz;

import com.dmbjz.client.MessageConsumerImplClient;
import com.dmbjz.client.NettyClient;
import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.dsl.ProducerType;
import disruptor.MessageConsumer;
import disruptor.RingBufferWorkerPoolFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NettyClientApplication {

    public static void main(String[] args) {

        SpringApplication.run(NettyClientApplication.class, args);

        //创建消费者
        MessageConsumer[] conusmers = new MessageConsumer[4];
        for(int i =0; i < conusmers.length; i++) {
            MessageConsumer messageConsumer = new MessageConsumerImplClient("code:clientId:" + i);
            conusmers[i] = messageConsumer;
        }

        //初始化并启动disruptor
        RingBufferWorkerPoolFactory.getInstance().initAndStart(ProducerType.MULTI,
                1024*1024,
                //new YieldingWaitStrategy(),
                new BlockingWaitStrategy(),
                conusmers);

        //在启动的时候执行Client代码
        new NettyClient().sendData();

    }

}
