package com.dmbjz.client;

import entity.TranslatorData;
import factory.MarshallingCodeCFactory;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;


public class NettyClient {

	private Channel channel;

	public NettyClient() {
		this.connect();
	}

	private void connect() {

		EventLoopGroup workGroup = new NioEventLoopGroup();
		Bootstrap bootstrap = new Bootstrap();
		try {
			
			bootstrap.group(workGroup)
			.channel(NioSocketChannel.class)
			//表示缓存区动态调配（自适应）
			.option(ChannelOption.RCVBUF_ALLOCATOR, AdaptiveRecvByteBufAllocator.DEFAULT)
			.handler(new LoggingHandler(LogLevel.INFO))
			.handler(new ChannelInitializer<SocketChannel>() {
				@Override
				protected void initChannel(SocketChannel sc) throws Exception {
					ChannelPipeline pipeline = sc.pipeline();
					//添加Java实体类编解码器
					pipeline.addLast(MarshallingCodeCFactory.buildMarshallingEncoder());
					pipeline.addLast(MarshallingCodeCFactory.buildMarshallingDecoder());
					//添加自定义处理器
					pipeline.addLast(new ClientHandler());
				}
			});

			//绑定端口，同步等等请求连接
			ChannelFuture future = bootstrap.connect("127.0.0.1", 8765).sync();
			System.out.println("客户端开始连接....");
			
			//进行数据的发送, 但是首先我们要获取channel:
			this.channel = future.channel();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}


	/* 数据发送方法 */
	public void sendData(){

		for(int i =0; i <10; i++){
			TranslatorData request = new TranslatorData();
			request.setId("" + i);
			request.setName("请求消息名称 " + i);
			request.setMessage("请求消息内容 " + i);
			this.channel.writeAndFlush(request);
		}

	}

	

}
