package com.dmbjz.client;

import disruptor.MessageConsumer;
import disruptor.TranslatorDataWrapper;
import entity.TranslatorData;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.ReferenceCountUtil;


public class MessageConsumerImplClient extends MessageConsumer {

	public MessageConsumerImplClient(String consumerId) {
		super(consumerId);
	}

	@Override
	public void onEvent(TranslatorDataWrapper event) throws Exception {

		TranslatorData response = event.getTranslatorData();
		//业务逻辑处理:
		try {
    		System.out.println("Client端: id= " + response.getId() + ", name= " + response.getName() + ", message= " + response.getMessage());
		} finally {
			ReferenceCountUtil.release(response);
		}

	}

}
