package com.dmbjz.client;

import disruptor.MessageProductor;
import disruptor.RingBufferWorkerPoolFactory;
import entity.TranslatorData;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;


public class ClientHandler extends ChannelInboundHandlerAdapter {

	@Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

		/*
				try {
					TranslatorData response = (TranslatorData)msg;
					System.err.println("Client端: id= " + response.getId()
							+ ", name= " + response.getName()
							+ ", message= " + response.getMessage());
				} finally {
					//释放缓存
					ReferenceCountUtil.release(msg);
				}
		*/

        TranslatorData response = (TranslatorData)msg;
        String producerId = "code:seesionId:002";
        MessageProductor messageProducer = RingBufferWorkerPoolFactory.getInstance().getMessageProducer(producerId);
        messageProducer.onData(response, ctx);


    }

}
