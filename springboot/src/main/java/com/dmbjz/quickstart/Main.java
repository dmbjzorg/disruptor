package com.dmbjz.quickstart;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

import java.nio.ByteBuffer;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;



public class Main {


    public static void main(String[] args) {


        OrderEventFactory orderEventFactory = new OrderEventFactory();
        int ringBufferSize = 1024*1024;
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(20,30,300, TimeUnit.MICROSECONDS,new LinkedBlockingDeque<>());

        /*
         * 实例化Disruptor对象，指定实际需要处理的事件对象
         * @param orderEventFactory事件工厂
         * @param ringBufferSize容器长度，必须为2的次幂
         * @param threadPool线程池
         * @param ProducerType指定生产者模式为单个或多个
         * @param WaitStrategy等待策略
         */
        Disruptor<OrderEvent> disruptor = new Disruptor<OrderEvent>(orderEventFactory,ringBufferSize,threadPool,ProducerType.SINGLE,new BlockingWaitStrategy());

        //添加消费者监听，获取生产者投递的数据 (构建disruptor 与 消费者的一个关联关系)
        disruptor.handleEventsWith(new OrderEventHandler());

        //启动disruptor
        disruptor.start();

        //获取实际存储数据的容器
        RingBuffer<OrderEvent> ringBuffer = disruptor.getRingBuffer();

        //创建生产者
        OrderEventProducer producer = new OrderEventProducer(ringBuffer);

        //模拟投递100次数据
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        for (int i = 0; i < 100; i++) {
            byteBuffer.putInt(0,i);
            producer.send(byteBuffer);
        }

        //关闭框架与线程池
        disruptor.shutdown();
        threadPool.shutdown();

    }


}
