package com.dmbjz.quickstart;


import com.lmax.disruptor.RingBuffer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.nio.ByteBuffer;


/**生产者 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderEventProducer {

    private RingBuffer<OrderEvent> ringBuffer;

    //投递数据的办法
    public void send(ByteBuffer byteBuffer){

        //在生产者发送消息的时候, 首先需要从RingBuffer里面获取下一个可用的序号
        long sequence = ringBuffer.next();
        try {
            //通过序号找到具体数据对象( 此时获取的对象是一个空对象,相当于获得到一个 new OrderEvent()  )
            OrderEvent orderEvent = ringBuffer.get(sequence);
            //对传递过来的数据赋值到对象中(数据一直在0索引位)
            orderEvent.setValue(byteBuffer.getInt(0));
        } finally {
            //发布数据
            ringBuffer.publish(sequence);
        }

    }

}
