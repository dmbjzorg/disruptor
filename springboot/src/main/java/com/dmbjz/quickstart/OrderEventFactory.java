package com.dmbjz.quickstart;

import com.lmax.disruptor.EventFactory;


/** Event工厂,用于创建对象实例，需要实现EventFactory接口 */
public class OrderEventFactory implements EventFactory<OrderEvent> {

    //这个方法就是为了返回空的消息&数据对象（Event）
    @Override
    public OrderEvent newInstance() {
        return new OrderEvent();
    }

}
