package com.dmbjz.quickstart;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/** 消息数据对象 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderEvent {

    private Integer value; //订单价格

}
