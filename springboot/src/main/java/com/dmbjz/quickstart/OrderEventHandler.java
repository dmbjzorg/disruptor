package com.dmbjz.quickstart;


import com.lmax.disruptor.EventHandler;


/**监听事件类，用于处理数据（Event类），可以理解为消费者 */
public class OrderEventHandler implements EventHandler<OrderEvent> {


    /* 事件监听模式 */
    @Override
    public void onEvent(OrderEvent orderEvent, long l, boolean b) throws Exception {
        System.out.println("消费者: " + orderEvent.getValue());
    }


}
