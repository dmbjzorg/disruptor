package com.dmbjz.height.chain;

import com.lmax.disruptor.EventHandler;

import java.util.concurrent.TimeUnit;


/* 监听事件 */
public class UserHandler5 implements EventHandler<Trade> {

	@Override
	public void onEvent(Trade event, long sequence, boolean endOfBatch) throws Exception {
		System.out.println("UserHandler5 : GET PRICE: " +  event.getPrice());
		TimeUnit.SECONDS.sleep(1);
		event.setPrice(event.getPrice() + 3.0);
	}

}
