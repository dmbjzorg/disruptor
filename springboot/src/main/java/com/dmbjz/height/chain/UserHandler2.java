package com.dmbjz.height.chain;

import com.lmax.disruptor.EventHandler;

import java.util.UUID;
import java.util.concurrent.TimeUnit;


/* 监听事件 */
public class UserHandler2 implements EventHandler<Trade> {

	@Override
	public void onEvent(Trade event, long sequence, boolean endOfBatch) throws Exception {
		System.out.println("UserHandler2 : SET ID");
		TimeUnit.SECONDS.sleep(1);
		event.setId(UUID.randomUUID().toString());
	}

}
