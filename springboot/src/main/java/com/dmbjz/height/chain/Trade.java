package com.dmbjz.height.chain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.concurrent.atomic.AtomicInteger;

/* 需要操作的数据实体类 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Trade {

	private String id;
	private String name;
	private double price;
	private AtomicInteger count = new AtomicInteger(0);

}
