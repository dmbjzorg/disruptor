package com.dmbjz.height.chain;

import com.lmax.disruptor.dsl.Disruptor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.concurrent.CountDownLatch;


/* 生产者 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class TradePushlisher implements Runnable {

	private Disruptor<Trade> disruptor;
	private CountDownLatch latch;

	@Override
	public void run() {

		TradeEventTranslator eventTranslator = new TradeEventTranslator();
		for(int i =0; i < 1; i ++){
			//新的提交任务的方式
			disruptor.publishEvent(eventTranslator);			
		}
		latch.countDown();

	}

}
