package com.dmbjz.height.chain;

import com.lmax.disruptor.EventTranslator;

import java.util.Random;

/** 事件执行方法 */
public class TradeEventTranslator implements EventTranslator<Trade> {

    private Random random = new Random();

    @Override
    public void translateTo(Trade event, long sequence) {
        event.setPrice(random.nextDouble() * 9999);
    }

}
