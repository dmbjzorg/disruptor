package com.dmbjz.height.chain;

import com.lmax.disruptor.EventHandler;


/* 监听事件 */
public class UserHandler3 implements EventHandler<Trade> {

	@Override
	public void onEvent(Trade event, long sequence, boolean endOfBatch) throws Exception {
		System.out.println("UserHandler3 : NAME: "
								+ event.getName() 
								+ ", ID: " 
								+ event.getId()
								+ ", PRICE: " 
								+ event.getPrice()
								+ " INSTANCE : " + event.toString());
	}

}
