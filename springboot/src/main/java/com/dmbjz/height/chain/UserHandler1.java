package com.dmbjz.height.chain;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.WorkHandler;

import java.util.concurrent.TimeUnit;


/* 监听事件，使用 WorkHandler 与 EventHandler 进行事件监听
* 		WorkHandler 相较于 EventHandler仅需要一个参数
*/
public class UserHandler1 implements EventHandler<Trade>, WorkHandler<Trade>{

	//EventHandler方法
	@Override
	public void onEvent(Trade event, long sequence, boolean endOfBatch) throws Exception {
		this.onEvent(event);
	}

	//WorkHandler方法
	@Override
	public void onEvent(Trade event) throws Exception {
		System.out.println("UserHandler1  : SET NAME");
		TimeUnit.SECONDS.sleep(1);
		event.setName("H1");
	}

}
