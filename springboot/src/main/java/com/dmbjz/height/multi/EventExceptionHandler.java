package com.dmbjz.height.multi;


import com.lmax.disruptor.ExceptionHandler;


/* 事件处理失败时的操作 */
public class EventExceptionHandler implements ExceptionHandler<Order> {

    @Override
    public void handleEventException(Throwable ex, long sequence, Order event) {
        System.out.println("消费时出现异常");
    }

    @Override
    public void handleOnStartException(Throwable ex) {
        System.out.println("启动时出现异常");
    }

    @Override
    public void handleOnShutdownException(Throwable ex) {
        System.out.println("关闭时出现异常");
    }

}
