package com.dmbjz.height.multi;

import lombok.Data;
import lombok.NoArgsConstructor;

/* Disruptor中的 Event */
@Data
@NoArgsConstructor
public class Order {

	private String id;
	private String name;
	private double price;
	
}
