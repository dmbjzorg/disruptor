package com.dmbjz.height.multi;

import com.lmax.disruptor.RingBuffer;


/* 旧版的生产者发布数据方式 */
public class OldProducer {
	
	private RingBuffer<Order> ringBuffer;
	
	public OldProducer(RingBuffer<Order> ringBuffer) {
		this.ringBuffer = ringBuffer;
	}

	public void sendData(String uuid) {
		long sequence = ringBuffer.next();
		try {
			Order order = ringBuffer.get(sequence);
			order.setId(uuid);
		} finally {
			ringBuffer.publish(sequence);
		}
	}

}
