package com.dmbjz.height.multi;


import com.lmax.disruptor.*;
import com.lmax.disruptor.dsl.ProducerType;

import java.util.UUID;
import java.util.concurrent.*;


/**多生产者模式案例
 *  Disruptor默认情况下为单生产者
 *  多生产者模式下需要自定义 RingBuffer、SequenceBarrier
 */
public class MultiMain {


    public static void main(String[] args) throws InterruptedException {

        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(8,16,10, TimeUnit.SECONDS,new ArrayBlockingQueue<>(20));

        //对RingBuffer设置多生产者支持
        RingBuffer<Order> ringBuffer = RingBuffer.create(ProducerType.MULTI, () -> new Order(), 1024 * 1024, new YieldingWaitStrategy());

        //通过RingBuffer创建一个屏障,用于保持对 RingBuffer 的 Producer 和 Consumer 之间的平衡关系
        SequenceBarrier barrier =  ringBuffer.newBarrier();

        //构建多消费者
        Consumer[] consumers = new Consumer[10];
        for(int i = 0; i < consumers.length; i++) {
            consumers[i] = new Consumer("C" + i);
        }

        //构建多消费者工作池
        WorkerPool<Order> workerPool = new WorkerPool<Order>(ringBuffer,barrier, new EventExceptionHandler(),consumers);

        //每个消费者的Sequence序号都是单独的，通过WorkPool获取每个消费者的序号然后设置到RingBuffer中
        ringBuffer.addGatingSequences(workerPool.getWorkerSequences());

        //启动工作池
        workerPool.start(threadPool);

        //阻塞
        CountDownLatch countDownLatch = new CountDownLatch(1);

        //创建100个生产者
        for (int i = 0; i < 100; i++) {

            //使用旧版方法进行生成者数据发布
            OldProducer producer = new OldProducer(ringBuffer);
            new Thread(()->{

                try {
                    countDownLatch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int j = 0; j < 100; j++) {
                    producer.sendData(UUID.randomUUID().toString());
                }

            }).start();

        }

        TimeUnit.SECONDS.sleep(2);
        System.out.println("----------线程创建完毕，开始生产数据----------");
        countDownLatch.countDown();

        TimeUnit.SECONDS.sleep(10);

        System.out.println("任务总数:" + consumers[3].getCount());



    }



}
