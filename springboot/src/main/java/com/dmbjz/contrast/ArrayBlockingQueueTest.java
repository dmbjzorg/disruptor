package com.dmbjz.contrast;

import java.util.concurrent.ArrayBlockingQueue;


/* ArrayBlockingQueue性能测试
*   模拟往其中放入一亿数据并取出需要花费的时间
*/
public class ArrayBlockingQueueTest {

    public static void main(String[] args) {

        ArrayBlockingQueue<MessageInfo> queue = new ArrayBlockingQueue<MessageInfo>(100000000);
        long startTime = System.currentTimeMillis();

        //添加元素
        new Thread(()->{
            long i = 0;
            while (i < 100000000) {
                MessageInfo data = new MessageInfo(i, "c" + i);
                try {
                    queue.put(data);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i++;
            }
        }).start();

        //取出元素
        new Thread(()->{
            int k = 0;
            while (k < 100000000) {
                try {
                    queue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                k++;
            }
            long endTime = System.currentTimeMillis();
            System.out.println("ArrayBlockingQueue 花费时间 = " + (endTime - startTime) + "ms");
        }).start();

    }


}