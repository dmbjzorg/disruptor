package com.dmbjz.contrast;

import com.lmax.disruptor.BlockingWaitStrategy;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.YieldingWaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

import java.util.concurrent.Executors;

public class DisruptorTest {

    public static void main(String[] args) {

        int ringBufferSize = 1024*1024;

        Disruptor<MessageInfo> disruptor = new Disruptor<MessageInfo>(
                () -> new MessageInfo(),
                ringBufferSize,
                Executors.newSingleThreadExecutor(),
                ProducerType.SINGLE,
                new YieldingWaitStrategy()
        );

        DataConsumer consumer = new DataConsumer();
        //消费数据
        disruptor.handleEventsWith(consumer);
        //启用框架
        disruptor.start();

        new Thread(()->{
            RingBuffer<MessageInfo> ringBuffer = disruptor.getRingBuffer();
            for (long i = 0; i < 100000000; i++) {
                long seq = ringBuffer.next();
                MessageInfo data = ringBuffer.get(seq);
                data.setId(i);
                data.setName("c" + i);
                ringBuffer.publish(seq);
            }
        }).start();


    }


}