package com.dmbjz.contrast;

import com.lmax.disruptor.EventHandler;

public class DataConsumer implements EventHandler<MessageInfo> {

    private long startTime;
    private int i;

    public DataConsumer() {
        this.startTime = System.currentTimeMillis();
    }

    @Override
    public void onEvent(MessageInfo data, long seq, boolean bool)
            throws Exception {
        i++;
        if (i == 100000000) {
            long endTime = System.currentTimeMillis();
            System.out.println("Disruptor costTime = " + (endTime - startTime) + "ms");
        }
    }

}
